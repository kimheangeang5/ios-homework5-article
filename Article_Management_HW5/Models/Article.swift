//
//  Article.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/18/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import Foundation
import ObjectMapper
class Article: Mappable{
    var id : Int?
    var title: String?
    var image: String?
    var content: String?
    var date : String?
    required init?(map: Map) {
        
    }
    init(){
        
    }
    func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        content <- map["DESCRIPTION"]
        image <- map["IMAGE"]
        date <- map["CREATED_DATE"]
    }
}
