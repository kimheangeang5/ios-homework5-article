//
//  ArticleTableViewCell.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/18/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var imageContainer: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func prepareCell(article: Article){
        titleLabel.text = article.title
        var originDate = article.date!
        let c = originDate.characters
        let y = c.index(c.startIndex, offsetBy: 0)..<c.index(c.endIndex, offsetBy: -10)
        let m = c.index(c.startIndex, offsetBy: 4)..<c.index(c.endIndex, offsetBy: -8)
        let d = c.index(c.startIndex, offsetBy: 6)..<c.index(c.endIndex, offsetBy: -6)
        let year = originDate[y]
        let month = originDate[m]
        let day = originDate[d]
        let dateString = day + "-" + month + "-" + year
        dateLabel.text = dateString
        authorLabel.text = "Sabay"
        let i = Int.random(in: 50 ... 600)
         let j = Int.random(in: 40 ... 600)
        shareLabel.text = String(i)
        viewLabel.text = String(j)
        guard let url = URL(string: article.image ?? "") else{return}
        imageContainer.kf.setImage(with: url, placeholder: UIImage(named: "default_img"))
    }
}
