//
//  ViewController.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/18/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//
// Save Image, Scroll view, loading when add new article

import UIKit
import Alamofire
import MBProgressHUD

class ViewController: UIViewController,UITableViewDataSource{
    @IBOutlet weak var tableViewArticle: UITableView!
    var refreshControl = UIRefreshControl()
    var articles = [Article]()
    var articlePresenter: ArticlePresenter?
    var page = 1
    var totalpage = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        articlePresenter = ArticlePresenter()
        articlePresenter?.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableViewArticle.addSubview(refreshControl)
        let view = UIView()
        tableViewArticle.addSubview(view)
    }
    @objc func refresh() {
        page = 1
        articles = []
        articlePresenter?.getArticle(withPage: page, withLimitation: 15)
    }
    override func viewWillAppear(_ animated: Bool) {
        //page = 1
        articles = []
        articlePresenter?.getArticle(withPage: page, withLimitation: 15)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! ArticleTableViewCell
        if indexPath.row > -1 || indexPath.row < articles.count {
            cell.prepareCell(article: articles[indexPath.row])
        }
        return cell
    }
    
    @IBAction func addNewArticle(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addNew") as! AddNewArticleViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    private func showLoadingHUD() {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "Please wait..."
    }
    
    private func hideLoadingHUD() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            self.articlePresenter?.deleteArticle(id: self.articles[indexPath.row].id!)
            self.articles.remove(at: indexPath.row)
            self.tableViewArticle.deleteRows(at: [indexPath], with: .fade)
            self.tableViewArticle.reloadData()
            print("Delete Action Tapped")
        }
        deleteAction.backgroundColor = .red
        deleteAction.image = UIImage(named: "delete")
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .destructive, title: "Edit") { (action, view, handler) in
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addNew") as! AddNewArticleViewController
            vc.isUpdate = true
            vc.articleUpdate = self.articles[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        editAction.backgroundColor = .green
        editAction.image = UIImage(named: "edit")
        let configuration = UISwipeActionsConfiguration(actions: [editAction])
        return configuration
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detail") as! ViewDetailViewController
        vc.article = self.articles[indexPath.row]
        print(indexPath.row)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = articles.count - 1
        if indexPath.row == lastElement {
            page += 1
            print(totalpage)
            if page <= totalpage {
                showLoadingHUD()
                articlePresenter?.getArticle(withPage: page, withLimitation: 15)
            }
            else{
                page = totalpage
                print("The last page")
            }
        }
    }
}

extension ViewController : ArticlePresenterProtocol{
    func didAddArticle() {
        
    }
    
    func didResponseArticle(article: [Article],totalPage: Int) {
        self.articles = self.articles + article
        self.totalpage = totalPage
        refreshControl.endRefreshing()
        hideLoadingHUD()
        tableViewArticle.reloadData()
    }
    
    func didResponseMessage(msg: String) {
        
    }
    
    
}
