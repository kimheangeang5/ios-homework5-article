//
//  ViewDetailViewController.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/20/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import Kingfisher
class ViewDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageContainer: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    var article : Article!
    override func viewDidLoad() {
        super.viewDidLoad()
        // show detail
        titleLabel.text = article.title
        contentLabel.text = article.content
        guard let url = URL(string: article.image ?? "") else{return}
        imageContainer.kf.setImage(with: url, placeholder: UIImage(named: "default_img"))
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.3
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.imageContainer.addGestureRecognizer(lpgr)
        imageContainer.isUserInteractionEnabled = true
    }
}

extension ViewDetailViewController : UIGestureRecognizerDelegate {
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        let questionController = UIAlertController()
        questionController.addAction(UIAlertAction(title: "Save", style: .default, handler: {
            (action:UIAlertAction!) -> Void in
          //Save Image
            UIImageWriteToSavedPhotosAlbum(self.imageContainer.image!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }))
        questionController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(questionController, animated: true, completion: nil)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
}
