//
//  AddNewArticleViewController.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/18/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
class AddNewArticleViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageViewContainer: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var actionButton: UIBarButtonItem!
    var articlePresenter: ArticlePresenter?
    var isUpdate = false
    var articleUpdate : Article!
    var imagePicker = UIImagePickerController()
    let header : HTTPHeaders = [
        "Authorization":"Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
        ]
    override func viewDidLoad() {
        super.viewDidLoad()
        articlePresenter = ArticlePresenter()
        articlePresenter?.delegate = self
        imagePicker.delegate = self
        imageViewContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseImage)))
        imageViewContainer.isUserInteractionEnabled = true
        if isUpdate {
            showUpdateArticle()
            actionButton.title = "Update"
        }
    }
    @IBAction func chooseImageTap(_ sender: Any) {
        chooseImage()
    }
    @IBAction func addNewArticleTap(_ sender: Any) {
        addArticle()
    }
    func showUpdateArticle(){
        titleTextField.text = articleUpdate.title
        contentTextView.text = articleUpdate.content
        guard let url = URL(string: articleUpdate.image ?? "") else{return}
        imageViewContainer.kf.setImage(with: url, placeholder: UIImage(named: "default_img"))
    }
    @objc func chooseImage(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageViewContainer.contentMode = .scaleToFill
            imageViewContainer.image = pickedImage
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    func addArticle () {
        let article = Article()
        article.title = titleTextField.text
        article.content = contentTextView.text
        if isUpdate{
            article.id = articleUpdate.id
        }
        articlePresenter?.addArticle(article: article,image: imageViewContainer.image!,isUpdate: isUpdate)
        showLoadingHUD()
    }
    
    private func showLoadingHUD() {
        let hud = MBProgressHUD.showAdded(to: contentView, animated: true)
        hud.label.text = "Please wait..."
    }
    
    private func hideLoadingHUD() {
        MBProgressHUD.hide(for: contentView, animated: true)
    }
}
extension AddNewArticleViewController: ArticlePresenterProtocol{
    func didAddArticle() {
        hideLoadingHUD()
        navigationController?.popViewController(animated: true)
    }
    
    func didResponseArticle(article: [Article], totalPage: Int) {
        
    }
    
    func didResponseMessage(msg: String) {
        print("Success Add / Update")
    }
    
    
}
