//
//  ArticleService.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/25/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import Foundation
import Alamofire

protocol ArticleServiceProtocol {
    func didResponseArticle(article: [Article], totalPage: Int)
    func didResponseMessage(msg: String)
    func didAddArticle()
}

class ArticleService{
    var delegate: ArticleServiceProtocol?
    var article_get_url = "http://api-ams.me/v1/api/articles"
    let header = [
        "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
        "Accept": "application/json",
        "Content-Type": "application/json"
    ]
    func getArticle(withPage: Int, withLimitation: Int){
        var article =  [Article]()
        Alamofire.request("\(article_get_url)?page=\(withPage)&limit=\(withLimitation)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (data) in
            if data.result.isSuccess {
                let res = data.result.value as! [String:Any]
                let valueObj = res["DATA"] as! NSArray
                let pagination = res["PAGINATION"] as! [String:Any]
                let totalpage = pagination["TOTAL_PAGES"] as! Int
                for i in valueObj{
                    let a = i as! [String:Any]
                    article.append(Article(JSON: a)!)
                }
                self.delegate?.didResponseArticle(article: article,totalPage: totalpage)
            }
            else{
                
            }
        }
        
    }

    func addArticle(article: Article,image: UIImage,isUpdate: Bool){
        var url = ""
        var method : HTTPMethod!
        if isUpdate{
            url = "\(article_get_url)/\(article.id!)"
            method = HTTPMethod.put
        }
        else{
            url = article_get_url
            method = HTTPMethod.post
        }
        //Upload File
        Alamofire.upload(multipartFormData: { (d) in
            d.append((image.jpegData(compressionQuality: 0.2))!, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: "http://api-ams.me/v1/api/uploadfile/single", method : .post, headers : header) { (result) in
            switch result {
            case .success(request: let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if let data = try? JSONSerialization.jsonObject(with: response.data!, options: []) as! [String:Any]{
                        // Add to Article
                        let parameters: [String: Any] = [
                            "TITLE": article.title!,
                            "DESCRIPTION": article.content!,
                            "IMAGE": data["DATA"] as! String
                        ]
                        Alamofire.request(url, method: method, parameters: parameters,encoding: JSONEncoding.default, headers: self.header).responseJSON { (res) in
                                print(res.result.value!)
                            self.delegate?.didAddArticle()
                        }
                    }
                })
            case .failure(let e):
                print(e)
                print("Upload failed!!!")
            }
        }
    }
    func deleteArticle(id: Int){
         Alamofire.request("\(article_get_url)/\(id)",
            method: .delete).responseJSON { (res) in
                if res.result.isSuccess{
                    print("Delete Success!")
                }
        }
    }
}
