//
//  ArticlePresenter.swift
//  Article_Management_HW5
//
//  Created by Kimheang on 12/25/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import Foundation
import UIKit

protocol ArticlePresenterProtocol {
    func didResponseArticle(article: [Article],totalPage: Int)
    func didResponseMessage(msg: String)
    func didAddArticle()
}

class ArticlePresenter: ArticleServiceProtocol {
    
    var delegate: ArticlePresenterProtocol?
    var articleService: ArticleService?
    
    init(){
        self.articleService = ArticleService()
        self.articleService?.delegate = self
    }
    
    func getArticle(withPage: Int, withLimitation: Int){
        self.articleService?.getArticle(withPage: withPage, withLimitation: withLimitation)
    }
    
    func addArticle(article: Article,image: UIImage,isUpdate: Bool){
        self.articleService?.addArticle(article: article,image: image,isUpdate: isUpdate)
    }
    func deleteArticle(id: Int){
        self.articleService?.deleteArticle(id: id)
    }
    func didResponseArticle(article: [Article],totalPage: Int) {
        self.delegate?.didResponseArticle(article: article,totalPage: totalPage)
    }
    
    func didResponseMessage(msg: String) {
        self.delegate?.didResponseMessage(msg: msg)
    }
    func didAddArticle() {
        self.delegate?.didAddArticle()
    }
}

